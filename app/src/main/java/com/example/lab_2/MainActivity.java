package com.example.lab_2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;
    static String selectedUrl = "https://www.tek.no/feeds/general.xml";
    static int UpdateFrequency = 2; // 0=10min, 1=60min, 2=once a day
    static int ItemNoLimit = 20;
    static ArrayList<Article> articles = new ArrayList<>();
    Timestamp lastfetch, nextfetch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button preferenceButton = findViewById(R.id.btn_prefs);
        final Button newsListButton = findViewById(R.id.btn_newslist);

        loadData();  //Load saved data to receive saved state

        Timestamp current = new Timestamp(System.currentTimeMillis());  //Get current time
        if(nextfetch != null && nextfetch.before(current)) {   //Check if the planned fetch is in the present, if so, fetch now
            System.out.println(nextfetch.toString() + " er før " + current.toString() + "  Fetcher!!!");
            RSSFeedParser feedParser = new RSSFeedParser();  //Parse the feed to get fresh articles
            articles = feedParser.parseFeed(selectedUrl, ItemNoLimit);
            long now = System.currentTimeMillis(); //Get current time
            lastfetch = new Timestamp(now); //Set last fetch to now
            TextView lastfetc = findViewById(R.id.txt_lastfetch);
            lastfetc.setText(lastfetch.toString());
            TextView nextfetc = findViewById(R.id.txt_nextfetch);
            switch (UpdateFrequency) {  //Check what the update frequency for fetching is
                case 0: {   //If 0, every 10 minutes
                    nextfetch = new Timestamp(now+10*60*1000);
                    break;
                }
                case 1: {   //If 1, every 60 minutes
                    nextfetch = new Timestamp(now+60*60*1000);
                    break;
                }
                case 2: {   //If 2, every 24 hours
                    nextfetch = new Timestamp(now+24*60*60*1000);
                    break;
                }
            }
            nextfetc.setText(nextfetch.toString());
            saveData(); //Save the current data.
        }




        preferenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //Click listener for Preference activity button
                Intent intent = new Intent(MainActivity.this, PreferencesActivity.class);
                intent.putExtra("url", selectedUrl);
                intent.putExtra("frequency", UpdateFrequency);
                intent.putExtra("limit", ItemNoLimit);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        newsListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //Click listener for news list activity button
                System.out.println(articles.size());
                Intent intent = new Intent(MainActivity.this, NewsListActivity.class);
                Bundle extra = new Bundle();
                extra.putSerializable("articles", articles);        //Bundle up the fetched articles
                intent.putExtra("articles", extra);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) { //Execute this when preferences are saved
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                UpdateFrequency = data.getIntExtra("UpdateFrequency", 2); //Update fetch frequency
                selectedUrl = data.getStringExtra("selectedUrl");     //Update RSS Url
                ItemNoLimit = data.getIntExtra("ItemNoLimit", 10);  //Update number Limit for list
                Bundle extra = data.getBundleExtra("extras");
                articles = extra.getParcelableArrayList("articles");   //Get the articles
                long now = System.currentTimeMillis();  //Get current time
                lastfetch = new Timestamp(now);     //Update last fetched
                TextView lastfetc = findViewById(R.id.txt_lastfetch);
                lastfetc.setText(lastfetch.toString());
                TextView nextfetc = findViewById(R.id.txt_nextfetch);
                switch (UpdateFrequency) {  //Update time for next fetch according to preferences
                    case 0: {
                        nextfetch = new Timestamp(now+10*60*1000);
                        break;
                    }
                    case 1: {
                        nextfetch = new Timestamp(now+60*60*1000);
                        break;
                    }
                    case 2: {
                        nextfetch = new Timestamp(now+24*60*60*1000);
                        break;
                    }
                }
                nextfetc.setText(nextfetch.toString());
                saveData();     //Save the data
            }
        }

    }

    public void saveData() {        //Function for saving data
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();     //Gson object for making ArrayList into Json string
        String json = gson.toJson(articles);
        String lastf = lastfetch.toString();
        String nextf = nextfetch.toString();
        String URL = selectedUrl;
        int frequency = UpdateFrequency; // 0=10min, 1=60min, 2=once a day
        int noLimit = ItemNoLimit;
        editor.putString("articles", json);         //Put data into editor
        editor.putString("lastfetch", lastf);
        editor.putString("nextfetch", nextf);
        editor.putString("URL", URL);
        editor.putInt("frequency", frequency);
        editor.putInt("noLimit", noLimit);
        editor.apply();                 //Apply changes
        System.out.println("Data saved!");
    }

    public void loadData() {        //Function for loading saved data
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = preferences.getString("articles", null);
        try {
            lastfetch = Timestamp.valueOf(preferences.getString("lastfetch", null));
            nextfetch = Timestamp.valueOf(preferences.getString("nextfetch", null));
            Type type = new TypeToken<ArrayList<Article>>() {
            }.getType();
            if (json != null) {
                articles = gson.fromJson(json, type);
            }
            System.out.println("Data loaded!");
            TextView lastfet = findViewById(R.id.txt_lastfetch);
            lastfet.setText(lastfetch.toString());
            TextView nextfet = findViewById(R.id.txt_nextfetch);
            nextfet.setText(nextfetch.toString());
            selectedUrl = preferences.getString("URL", "");
            UpdateFrequency = preferences.getInt("frequency", 2);
        } catch (RuntimeException e) {
            e.printStackTrace();
            TextView lastfetch = findViewById(R.id.txt_lastfetch);
            lastfetch.setText("Never, go to Preferences to set up.");
            TextView nextfetch = findViewById(R.id.txt_nextfetch);
            nextfetch.setText("Never going to happen.");

        }
    }
}
