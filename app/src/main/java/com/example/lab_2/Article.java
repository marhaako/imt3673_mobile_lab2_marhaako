package com.example.lab_2;

import android.os.Parcel;
import android.os.Parcelable;

public class Article implements Parcelable {

    final String title;
    final String link;
    final String description;
    final String image;
    final String pubDate;


    //Constructor
    public Article(String stitle, String slink, String sdescription, String simage, String spubdate) {
        title = stitle;
        link = slink;
        description = sdescription;
        image = simage;
        pubDate = spubdate;

    }

    public String getTitle() { return title; }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getImage(){ return image; }

    public String getPubDate(){ return pubDate; }





    protected Article(Parcel in) {
        title = in.readString();
        link = in.readString();
        description = in.readString();
        image = in.readString();
        pubDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(pubDate);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
}