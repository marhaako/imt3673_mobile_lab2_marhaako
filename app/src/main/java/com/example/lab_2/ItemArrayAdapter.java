package com.example.lab_2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class ItemArrayAdapter extends RecyclerView.Adapter<ItemArrayAdapter.ViewHolder> {

    private int listItemLayout;
    private ArrayList<Article> itemList;
    private static Context context;
    // Constructor of the class
    public ItemArrayAdapter(int layoutId, ArrayList<Article> itemList, Context context) {
        listItemLayout = layoutId;
        this.itemList = itemList;
        this.context = context;
    }



    // get the size of the list
    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(listItemLayout, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView item1 = holder.item1;
        TextView item2 = holder.item2;
        TextView item3 = holder.item3;
        holder.link = itemList.get(listPosition).getLink();
        item1.setText(itemList.get(listPosition).getTitle());
        item2.setText(itemList.get(listPosition).getDescription());
        item3.setText(itemList.get(listPosition).getPubDate());
         
    }


    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView item1;
        public TextView item2;
        public TextView item3;
        public String link;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            item1 = itemView.findViewById(R.id.txt_title);
            item2 = itemView.findViewById(R.id.txt_description);
            item3 = itemView.findViewById(R.id.txt_pubDate);
        }
        @Override
        public void onClick(View view) {
            Log.d("onclick", "onClick " + getLayoutPosition() + " " + link);
            Intent intent = new Intent(context, WebActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("link", link);
            context.startActivity(intent);
        }
    }

}