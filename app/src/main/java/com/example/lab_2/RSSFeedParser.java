package com.example.lab_2;


import android.graphics.Bitmap;
import android.text.Html;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class RSSFeedParser {


    public ArrayList<Article> parseFeed(final String RSSFeed, final int noOfArticles) {


        final ArrayList<Article> feed = new ArrayList<>();

            Thread t1 = new Thread() {

                    public void run () {
                        try {
                            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                            final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                            URL url = new URL(RSSFeed);
                            URLConnection urlc = url.openConnection();
                            urlc.addRequestProperty("User-Agent", "firefox");
                            Document doc = dBuilder.parse(urlc.getInputStream());
                            int limit = 0;

                            NodeList nList = doc.getElementsByTagName("item");

                            if(nList.getLength() < noOfArticles) {
                                limit = nList.getLength();
                            } else {
                                limit = noOfArticles;
                            }


                            for (int temp = 0; temp < limit; temp++) {

                                Node nNode = nList.item(temp);

                                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                                    Element eElement = (Element) nNode;
                                    String title = "";
                                    String description = "";
                                    String link = "";
                                    String image = "";
                                    String pubDate = "";
                                    if (eElement.getElementsByTagName("title").getLength() > 0) {
                                        title = eElement.getElementsByTagName("title").item(0).getTextContent();
                                        System.out.println("Title: " + title);
                                    }
                                    if (eElement.getElementsByTagName("description").getLength() > 0) {
                                        try {
                                            description = Html.fromHtml(eElement.getElementsByTagName("description").item(0).getTextContent()).toString();
                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("Desc: " + description);
                                    }
                                    if (eElement.getElementsByTagName("link").getLength() > 0) {
                                        link = eElement.getElementsByTagName("link").item(0).getTextContent();
                                        System.out.println("Link: " + link);
                                    }
                                    if (eElement.getElementsByTagName("image").getLength() > 0) {
                                        Node iNode = eElement.getElementsByTagName("image").item(0);
                                        Element iElement = (Element) iNode;
                                        image = iElement.getElementsByTagName("url").item(0).getTextContent();
                                        System.out.println("Image: " + image);
                                    }
                                    else if (eElement.getElementsByTagName("enclosure").getLength() > 0) {
                                        NamedNodeMap attr = eElement.getElementsByTagName("enclosure").item(0).getAttributes();
                                        image = attr.getNamedItem("url").getTextContent();
                                        System.out.println("Image2: " + image);
                                    }
                                    if (eElement.getElementsByTagName("pubDate").getLength() > 0) {
                                        pubDate = eElement.getElementsByTagName("pubDate").item(0).getTextContent();
                                    }

                                    feed.add(new Article(title, link, description, image, pubDate));
                                }
                            }
                        } catch (ParserConfigurationException | IOException | SAXException e) {
                            e.printStackTrace();
                            String error = "Ooops, something bad happened.";
                            feed.add(new Article(error, "", "Try fetching again and/or check your RSS link for flaws\n\nError: " + e.toString(), "", ""));
                        }
                    }
            };
            t1.start();

            try {
                t1.join();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

        return feed;
    }

}

