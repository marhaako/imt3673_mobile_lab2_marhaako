package com.example.lab_2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.regex.PatternSyntaxException;

public class NewsListActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);

        final TextView validation = findViewById(R.id.regex_validator);
        validation.setTextColor(Color.GREEN);
        validation.setText("Valid");

        Intent intent = this.getIntent();    //Get intent
        final TextView showing = findViewById(R.id.txt_showNoOfItems);  //Get the TextView to display number of articles to show
        final EditText regex = findViewById(R.id.txt_regex);  //Get the EditText field for the regular expression

        Bundle extra = intent.getBundleExtra("articles"); //Get the bundle from main activity
        final ArrayList<Article> articles = (ArrayList<Article>)extra.getSerializable("articles");  //Get the articles from the bundle

        String showingText = "Showing " + articles.size() + " items.";  //Set the number of items currently showing
        showing.setText(showingText);



        final ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(R.layout.list_item, articles, getApplicationContext()); //Create new adapter for recycler view with the articles in it
        recyclerView = findViewById(R.id.rv_news);
        recyclerView.setLayoutManager(new LinearLayoutManager(this)); //Make a layout manager for RV
        recyclerView.setItemAnimator(new DefaultItemAnimator());    //Make ItemAnimator for RV
        recyclerView.setAdapter(itemArrayAdapter);  //Set adapter for RecyclerView

        regex.addTextChangedListener(new TextWatcher() { //Listener for regexp field
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {  //Do this when regex field is edited
                String regexp = regex.getText().toString().toUpperCase();  //Get the regexp from edit field
                try {
                    final TextView validation = findViewById(R.id.regex_validator);
                    validation.setTextColor(Color.GREEN);
                    validation.setText("Valid");

                    RegexFilter regexFilter = new RegexFilter();   //Create a filtering object
                    final ArrayList<Article> filteredArticles = regexFilter.filter(articles, regexp); //Create an arraylist containing filtered articles

                    String showingText = "Showing " + filteredArticles.size() + " items.";   //Set the text of number of results
                    showing.setText(showingText);
                    final ItemArrayAdapter filter = new ItemArrayAdapter(R.layout.list_item, filteredArticles, getApplicationContext());  //Create new adapter with the filtered articles
                    recyclerView.setAdapter(filter);      //Set newly created adapter to recyclerview
                } catch (PatternSyntaxException e) {
                    e.printStackTrace();        //If the regex is invalid, catch the exception
                    final TextView validation = findViewById(R.id.regex_validator);
                    validation.setTextColor(Color.RED);
                    validation.setText("Invalid");
                    final ItemArrayAdapter invalid = new ItemArrayAdapter(R.layout.list_item, new ArrayList<Article>(), getApplicationContext());
                    recyclerView.setAdapter(invalid);
                    String showingText = "Showing " + 0 + " items due to invalid expression.";  //Set the number of items currently showing
                    showing.setText(showingText);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
