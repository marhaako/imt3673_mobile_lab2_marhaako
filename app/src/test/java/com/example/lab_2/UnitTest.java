package com.example.lab_2;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * UnitTest class for testing the parsing of RSSFeeds and
 * for testing the filtering of articles when using regex
 * expressions
 *
 */
public class UnitTest {

    private static String RSS1;
    private static String RSS2;
    private static ArrayList<Article> testArticles;
    private static RSSFeedParser testParser;
    private static RegexFilter testFilter;
    private static String invalidRegex;
    private static String validRegex;


    protected void setUp() {
        RSS1 = "https://www.tek.no/feeds/general.xml";
        RSS2 = "http://feeds.bbci.co.uk/news/technology/rss.xml#";
        testParser = new RSSFeedParser();
        testArticles  = new ArrayList<>();
        testFilter = new RegexFilter();
        invalidRegex = "*";
        validRegex = "";
        Article article = new Article("Apples nye Airpods støtter trådløs lading og har bedre batteri",
                "www.test.no",
                "Men var dette alt?",
                "www.imdb.com/123456/img.jpg",
                "2019-03-20 15:25:46.213");
        for (int i = 0; i < 19; i++) {
            testArticles.add(article);
        }
    }


    @Test
    public void regex_returnArticleWithDescriptionMatch() {
        setUp();
        Article descMatchArticle = new Article("test",
                "www.test.no",
                "Det fløy 500 droner i luften.",
                "www.test.no/img.jpg",
                "2019-03-20 15:25:46.213");
        testArticles.add(descMatchArticle);
        testArticles = testFilter.filter(testArticles, "[0-9]{2,}");
        assertEquals(1, testArticles.size());
    }

    @Test
    public void regex_returnArticleWithTitleMatch() {
        setUp();
        Article descMatchArticle = new Article("Derfor mener Nes-Bil det er nødvendig å flytte",
                "www.test.no",
                "Det fløy 500 droner i luften.",
                "www.test.no/img.jpg",
                "2019-03-20 15:25:46.213");
        testArticles.add(descMatchArticle);
        testArticles = testFilter.filter(testArticles, "([A-z]{3,})-\\D{3}");
        assertEquals(1, testArticles.size());
    }

    @Test
    public void parser_correctNumberOfArticlesReturned() {
        setUp();
        testArticles = testParser.parseFeed(RSS1, 20);
        assertEquals(20, testArticles.size());
        setUp();
        testArticles = testParser.parseFeed(RSS2, 10);
        assertEquals(10, testArticles.size());
    }
}