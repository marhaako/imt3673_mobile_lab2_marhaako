package com.example.lab_2;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFilter {


    //Method that takes a list of articles and a regular expression as arguments, and returns
    //an array list of matching articles
    public ArrayList<Article> filter(ArrayList<Article> articles, String regexp) {

        Pattern p = Pattern.compile(regexp);
        Matcher m1, m2;
        final ArrayList<Article> filteredArticles = new ArrayList<>();  //New arraylist for articles matching the regex
        for (int i = 0; i < articles.size(); i++) {   //Iterate through all articles
            m1 = p.matcher(articles.get(i).getTitle().toUpperCase());           //Create matcher for article title
            m2 = p.matcher(articles.get(i).getDescription().toUpperCase());     //Create matcher for article description
            if (m1.find()) {    //If hit on title
                System.out.println("Article: " + i + ", title match!");  //Log hit
                filteredArticles.add(articles.get(i));  //Add the article to the arraylist
            } else if (m2.find()) { //If hit on description
                System.out.println("Article: " + i + ", desc match!");
                filteredArticles.add(articles.get(i));
            }
        }

        return filteredArticles;
    }
}
