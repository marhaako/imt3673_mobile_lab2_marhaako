#Simple News Reader for RSS2.0

##Application

###Main Activity
The application contains a Main Activity where you can view when the last fetch happened and the next planned fetch. The time between these two corresponds to the fetching frequency specified in the Preferences activity. The Main Activity also contains two buttons, one leading to the News List Activity where you can view the fetched articles, and the other to the Preferences Activity.

###News List Activity
This activity contains a Recycler View populated with the fetched articles. Each item in the list contains a title and a description. They also contain a link to the actual article. By clicking each item, a new Activity is launched containing a Web View to the items link. In the very top of the News List Activity is an EditText field where you can write a regular expression. The article list is updated according to this expression. It will also tell you if the expression is valid or not. The application takes the regex and checks for matches in the title and description of each item. The match can be inside of the title and description as well, so you dont have to match the entire string.

###Preferences Activity
Here you can set your preferences for the fetching of articles. Use the spinners to set your desired update frequency and how many items you want to fetch. Use the EditText field to select your desired RSS Feed link. You will see that only RSS2.0 is supported. When you are ready you can click "Save and fetch" to save the preferences, and fetch the results. This will take you back to the Main Activity with a freshly updated "Last fetch" and "Next fetch" timestamp.

###The fetching
The initial fetch will happen when you press "Save and fetch" in the preferences tab. After that, the application will automatically refetch the articles when you re-enter the application and the "Next fetch" timestamp is in the past.

##Tests
Unit tests for the RSS parser and the Regex filtering functionality can be located in app/src/test/java/com/example/lab_2/UnitTest.java

When running the Unit Test for the parsing, for each item there will be a thrown exception when trying to get the description tag from the XML. This because I use a function called Html.fromHtml.toString to remove html tags etc, which sometimes returns null. The Parser returns the List of articles anyway, so the Test still serves its purpose. I found no way around this issue.
