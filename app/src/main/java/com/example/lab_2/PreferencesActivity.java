package com.example.lab_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class PreferencesActivity extends AppCompatActivity {


    static ArrayList<String> RSSUrls= new ArrayList<>();
    final Integer [] noOfArticlesValues = {10, 20, 50, 100};
    final String [] frequencyValues = {"10min", "60min", "once a day"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        Intent intent = this.getIntent();
        final int freq_default = intent.getIntExtra("frequency", 1);
        final int limit_default = intent.getIntExtra("limit", 10);
        final String selected_url = intent.getStringExtra("url");

        final Spinner noOfArticles = findViewById(R.id.spn_noOfArticles);
        final Spinner frequency = findViewById(R.id.spn_frequency);
        final EditText url = findViewById(R.id.edt_url);
        url.setText(selected_url);
        final Button save = findViewById(R.id.btn_save);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(
                getApplicationContext(),
                R.layout.spinner_item,
                noOfArticlesValues
        );
        noOfArticles.setAdapter(adapter);
        noOfArticles.setSelection(adapter.getPosition(limit_default)); //Sets the spinner position to the last saved value

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(
                getApplicationContext(),
                R.layout.spinner_item,
                frequencyValues
        );
        frequency.setAdapter(adapter2);
        frequency.setSelection(freq_default); //Sets the spinner position to the last saved value

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("UpdateFrequency", frequency.getSelectedItemPosition());
                intent.putExtra("ItemNoLimit", Integer.parseInt(noOfArticles.getSelectedItem().toString()));
                intent.putExtra("selectedUrl", url.getText().toString());
                RSSFeedParser feedParser = new RSSFeedParser();
                ArrayList<Article> articles = feedParser.parseFeed(url.getText().toString(), Integer.parseInt(noOfArticles.getSelectedItem().toString()));
                Bundle extras = new Bundle();
                extras.putParcelableArrayList("articles", articles);
                intent.putExtra("extras", extras);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }

        });
    }
}
